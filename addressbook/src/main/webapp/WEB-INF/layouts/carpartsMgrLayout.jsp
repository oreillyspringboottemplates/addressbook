<!DOCTYPE html>
<%@ include file="/WEB-INF/layouts/include.jsp"%>
<html lang="en">
<head>
<fmt:setBundle basename="app" var="app" />
<fmt:message key="jenkins.job" bundle="${app}" var="job" />
<fmt:message key="jenkins.buildNumber" bundle="${app}" var="buildNumber" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="job" content="${job}" />
<meta name="buildNumber" content="${buildNumber}" />
<title><tiles:getAsString name="title" /></title>

<fmt:setBundle basename="global" var="global" />
<fmt:message key="ui.url" bundle="${global}" var="uiurl" />
<fmt:message key="ui.bootstrap.4.current" bundle="${global}"
	var="latestBootstrap" />
<fmt:message key="ui.oreillyjs.1.current" bundle="${global}"
	var="oreillyjs" />

<c:set value="/resources/css/oreillybs-4.0.0r1.min.css"
	var="bootstrapUrl" />
<c:set value="/resources/js/oreillyjs/1.1.31/orly.js" var="oreillyjsUrl" />

<meta name="newUI" content="${bootstrapUrl}" />
<meta name="newJS" content="${oreillyjsUrl}" />

<!-- Bootstrap import - Check https://ui.oreillyauto.com/ui to make sure you have the latest! -->
<link rel="stylesheet" href="<c:url value='${bootstrapUrl}'/>">

<!-- Project CSS import -->
<link type="text/css"
	href="<c:url value='/resources/css/master.css?build=${buildNumber}' />"
	rel="stylesheet" />

<!-- O'Reilly JS -->
<script src="<c:url value='${oreillyjsUrl}?build=${buildNumber}' />"></script>
</head>

<body>
	<div id="bodyContentTile" class="container">
		<div class="row">
			<div class="col-sm-3">
				<tiles:insertAttribute name="carpartsMgrMenu" />
			</div>
			<div class="col-sm-9">
				<orly-alert-mgr id="alerts"></orly-alert-mgr>
				<tiles:insertAttribute name="body" />
			</div>
		</div>
	</div>
	<script>
	orly.ready.then(()=> {
		let message = "${message}";
		let messageType = "${messageType}";

		if (message.length > 0) {
			if (messageType.length > 0) {
				orly.qid("alerts").createAlert({type:"${messageType}", duration:"3000", msg:message});
			} else {
				orly.qid("alerts").createAlert({type:"info", duration:"3000", msg:message});
			}
		}
	});
</script>
</body>
</html>
