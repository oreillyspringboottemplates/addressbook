<%@ include file="/WEB-INF/layouts/include.jsp" %>

<h1>Address Book</h1>
<a class="btn btn-danger" href="/addressBook/addEditAddress">Add Address</a>
<orly-table id="addressBookTable" loaddataoncreate url="<c:url value='/addressBook/getaddresses' />" includefilter 
               bordered maxrows="10" tabletitle="Search Results" class="invisible">
	<orly-column field="action" label="Action" class="">
		<div slot="cell">
			<a href="\${`<c:url value='/addressBook/addEditAddress?id=\${model.id}' />`}">
				<orly-icon id="\${`e_\${model.id}`}" class="pointer" color="cornflowerblue" name='edit'></orly-icon>
			</a>
			<orly-icon id="\${`d_\${model.id}`}" class="pointer" color="red" name='x'></orly-icon>
	    </div>
	</orly-column>
	<orly-column field="id" label="ID" class="" sorttype="natural"></orly-column>
	<orly-column field="contactName" label="Contact Name" class=""></orly-column>
	<orly-column field="addressOne" label="Address One" class=""></orly-column>
	<orly-column field="addressTwo" label="Address Two" class=""></orly-column>
	<orly-column field="city" label="City" class=""></orly-column>
	<orly-column field="state" label="State" class=""></orly-column>
	<orly-column field="zip" label="Zip" class=""></orly-column>
</orly-table>

<script>
var table;

orly.ready.then(() => {
	table = orly.qid("addressBookTable");
	addEventListeners();
});

const addEventListeners = () => {
	orly.on(table,'click', (e) => {
		let id = e.target.id;
		if (id.startsWith('d_')) {
			id = id.substring(2);
	 		try {
 			let Address = {};
 			Address.id = id;
			
 			fetch("<c:url value='/addressBook/delete' />", {
 				method: "POST",
 				body: JSON.stringify(Address),
 				headers: {
 					"Content-Type": "application/json"
 				}
 			}).then((res) => {
 				if (res.ok) {
					return res.json();
 				} else {
 					console.error(res.status);
 				}
 			}).then((Response) => {
 				let message = Response.message;
 				let messageType = Response.messageType;
 				let duration = 3000;
 				orly.qid('alerts').createAlert({'msg': message, 'type': messageType, 'duration': duration});
 				table.loadData();
 			});
 		} catch (err) {
 			console.error(err);
 		}
		}
	});

// 	orly.on(orly.qid('btnSubmit'),'click', (e) => {
// 	});
// }
}
</script>