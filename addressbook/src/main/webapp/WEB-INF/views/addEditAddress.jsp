<%@ include file="/WEB-INF/layouts/include.jsp" %>
<h1>${title}</h1>
<%@ include file="/WEB-INF/views/message.jsp" %>
<form:form method="post" id="addressForm" modelAttribute="command" 
	action="${pageContext.request.contextPath}/addressBook/addEditAddress">
	<form:hidden path="id"/>
	<div class="form-group">
		<label for="contactName">Contact Name</label>
		<form:input path="contactName" class="form-control" placeholder="Contact Name"/>
	</div>	
	<div class="form-group">
		<label for="addressOne">Address One</label>
		<form:input path="addressOne" class="form-control" placeholder="Address One"/>
	</div>	
	<div class="form-group">
		<label for="addressTwo">Address Two</label>
		<form:input path="addressTwo" class="form-control" placeholder="Address Two"/>
	</div>	
	<div class="form-group">
		<label for="city">City</label>
		<form:input path="city" class="form-control" placeholder="City"/>
	</div>	
	<div class="form-group">
		<label for="state">State</label>
		<form:input path="state" class="form-control" placeholder="State"/>
	</div>	
	<div class="form-group">
		<label for="zip">Zip</label>
		<form:input path="zip" class="form-control" placeholder="Zip"/>
	</div>	
	<button class="btn btn-secondary" type="reset" id="btnReset">Reset</button>
	<a class="btn btn-danger" href="<c:url value='/addressBook' />" id="btnCancel">Cancel</a>
<%-- 	<a class="btn btn-danger" href="<%=request.getContextPath() %>" id="btnCancel">Cancel</a>
 --%>	<button class="btn btn-primary" type="button" id="btnSubmit">Submit</button>
</form:form>

<script>
	orly.ready.then(() => {
		addEventListeners();
	});
	
	const addEventListeners = () => {
		orly.on(orly.qid('btnSubmit'),'click', (e) => {
			try {
				let Address = {};
				Address.id = orly.qid('id').value;
				Address.contactName = orly.qid('contactName').value;
				Address.addressOne = orly.qid('addressOne').value;
				Address.addressTwo = orly.qid('addressTwo').value;
				Address.city = orly.qid('city').value;
				Address.state = orly.qid('state').value;
				Address.zip = orly.qid('zip').value;
				
				
				fetch("<c:url value='/addressBook/addEditAddress' />", {
					method: "POST",
					body: JSON.stringify(Address),
					headers: {
						"Content-Type": "application/json"
					}
				}).then((res) => {
					if (res.ok) {
						return res.json();
					} else {
						console.error(res.status);
					}
				}).then((Response) => {
					let message = Response.message;
					let messageType = Response.messageType;
					let duration = 3000;
					orly.qid('alerts').createAlert({'msg': message, 'type': messageType, 'duration': duration});
					setTimeout(() => {
						location.href="<c:url value='/addressBook' />";
					}, duration);
				});
			} catch (err) {
				console.error(err);
			}
		});
	}
</script>
