/*CREATE TABLE `addressbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_name` varchar(256) NOT NULL,
  `address_one` varchar(256) NOT NULL,
  `address_two` varchar(256) NOT NULL,
  `city` varchar(45) NOT NULL,
  `state` varchar(2) NOT NULL,
  `zip` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
*/

package com.oreillyauto.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "addressbOok")

public class Address {
	@Column(name="id", columnDefinition = "INTEGER")
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="contact_name", nullable=false,columnDefinition="VARCHAR(256)")
	private String contactName;//`contact_name` varchar(256) NOT NULL,
	
	@Column(name="address_one", nullable = false, columnDefinition = "VARCHAR(256)")
	private String addressOne;//`address_one` varchar(256) NOT NULL,
	
	@Column(name="address_two", nullable = false, columnDefinition = "VARCHAR(256)")
	private String addressTwo;//	  `address_two` varchar(256) NOT NULL,
	
	@Column(name="city", nullable = false, columnDefinition = "VARCHAR(45)")
	private String city;//	  `city` varchar(45) NOT NULL,
	
	@Column(name="state", nullable = false, columnDefinition = "VARCHAR(2)")
	private String state;//	  `state` varchar(2) NOT NULL,
	
	@Column(name="zip", nullable = false, columnDefinition = "VARCHAR(5)")
	private String zip;//	  `zip` varchar(5) NOT NULL,

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getAddressOne() {
		return addressOne;
	}

	public void setAddressOne(String addressOne) {
		this.addressOne = addressOne;
	}

	public String getAddressTwo() {
		return addressTwo;
	}

	public void setAddressTwo(String addressTwo) {
		this.addressTwo = addressTwo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}
}
