package com.oreillyauto.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.domain.Address;
import com.oreillyauto.model.Response;
import com.oreillyauto.service.AddressBookService;
import com.oreillyauto.util.Helper;

@Controller
public class AddressBookController {

	@Autowired
	AddressBookService addressBookService;

	@GetMapping(value = "/addressBook")
	public String getAddressBook() {
		return "addressBook";
	}

	@ResponseBody
	@GetMapping(value = "/addressBook/getaddresses")
	public List<Address> getAddressi() {
		return addressBookService.getAddressi();
	}

	@GetMapping(value = "/addressBook/addEditAddress")
	public String getAddEdit(HttpSession session, Model model, String id) {

		String page = "addEditAddress";

		// If the ID is an integer, we need to load the "edit user" page
		// Otherwise, we need to load the "add user" page
		if (Helper.isInteger(id)) {
			page = addressBookService.prepareEditAddressBook(session, model, Integer.parseInt(id));
		} else {
			page = addressBookService.prepareAddAddressBook(model);
		}

		// Return the
		return page;

	}
	
	@ResponseBody
	@PostMapping(value="/addressBook/addEditAddress")
	public Response postAddEdit(@RequestBody Address address) {
		Response response = new Response();
		
		try {
			String action = address.getId() == null ? "added" : "updated";
			addressBookService.addEditAddress(address);
			response.setMessage(address.getContactName() + " " + action + " successfully!");
			response.setMessageType("success");
			return response;
		} catch (Exception e) {
			response.setMessageType("danger");
			response.setMessage("Error processing request!");
			return response;
		}
	}
	
	@ResponseBody
	@PostMapping(value="/addressBook/delete")
	public Response postDelete(@RequestBody Address address) {
		Response response = new Response();
		
		try {
			addressBookService.deleteAddress(address);
			response.setMessage("Deleted succesfully");
			response.setMessageType("success");
			return response;
		} catch (Exception e) {
			response.setMessageType("danger");
			response.setMessage("Error processing request!");
			return response;
		}
	}
	
	/*
	 * @GetMapping(value="/addressBook/addEd") public String getEdit()
	 */

}
