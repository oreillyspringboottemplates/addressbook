package com.oreillyauto.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {

	
	@ResponseBody
	@GetMapping(value="/test/string")
	public String getTestString() {
		return "test";
	}
	
	@GetMapping(value="/test/page")
	public String getTestPage() {
		return "testpage";
	}
	
}
