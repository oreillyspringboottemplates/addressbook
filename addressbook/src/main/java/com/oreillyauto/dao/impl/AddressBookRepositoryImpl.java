package com.oreillyauto.dao.impl;

import java.util.List;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.AddressBookRepositoryCustom;
import com.oreillyauto.domain.Address;
import com.oreillyauto.domain.QAddress;

@Repository
public class AddressBookRepositoryImpl extends QuerydslRepositorySupport implements AddressBookRepositoryCustom  {
	// impl's everything in custom
	public AddressBookRepositoryImpl() {
		super(Address.class);
	}
	
	QAddress addressTable = QAddress.address;

	@Override
	public List<Address> getAddressi() {
		return from(addressTable).fetch();
	}

	@Override
	public Address getAddressById(Integer id) {
		// TODO Auto-generated method stub
		
		return (Address) from(addressTable)
				.where(addressTable.id.eq(id))
				.limit(1)
				.fetchOne();
	}
}
