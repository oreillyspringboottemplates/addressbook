package com.oreillyauto.dao.custom;

import java.util.List;

import com.oreillyauto.domain.Address;

public interface AddressBookRepositoryCustom {
	List<Address> getAddressi();
	
	Address getAddressById(Integer id);

}
