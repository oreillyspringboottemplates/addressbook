package com.oreillyauto.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;

import com.oreillyauto.domain.Address;

public interface AddressBookService {

	List<Address> getAddressi();

	String prepareEditAddressBook(HttpSession session, Model model, Integer parseInt);

	String prepareAddAddressBook(Model model);

	void addEditAddress(Address address);

	void deleteAddress(Address address);

}
