package com.oreillyauto.service.impl;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.oreillyauto.dao.AddressBookRepository;
import com.oreillyauto.domain.Address;
import com.oreillyauto.service.AddressBookService;

@Service("addressBookService")
public class AddressBookServiceImpl implements AddressBookService {
	
	@Autowired
	AddressBookRepository addressBookRepo;

	@Override
	public List<Address> getAddressi() {
		return addressBookRepo.getAddressi();
	}

	@Override
	public String prepareAddAddressBook(Model model) {
		// Create a new instance
		Address address  = new Address();
		
		// Add the empty user and title to the model
		model.addAttribute("command", address);
		model.addAttribute("title", "Add User");
		return "addEditAddress";
	}

	@Override
	public String prepareEditAddressBook(HttpSession session, Model model, Integer id) {
		// Get the user by ID
		Address address = getAddressById(id);
		
		if (address != null) {
			model.addAttribute("command", address);
			model.addAttribute("title", "Edit Address");
			return "addEditAddress";
		} else {
			// Provide error message to the user and prepare Add User
			session.setAttribute("message", "Sorry, address with ID = " + id + " not found.");
			return "redirect:/addressBook";
		}
	}

	private Address getAddressById(Integer id) {
		
		return addressBookRepo.getAddressById(id);
	}
	
	private Address getCrudAddressById(Integer id) {
		
		Optional<Address> addressOpt = addressBookRepo.findById(id);
		if (addressOpt.isPresent()) {
			return addressOpt.get();
		} else {
			return null;
		}
	}


	@Override
	public void addEditAddress(Address address) {
		addressBookRepo.save(address);
	}

	@Override
	public void deleteAddress(Address address) {
		addressBookRepo.delete(address);
	}


}
