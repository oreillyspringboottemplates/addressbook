package com.oreillyauto.model;

import java.io.Serializable;

public class Response implements Serializable {

	private static final long serialVersionUID = 166097742688638095L;

	public Response() {}
	
	private String message;
	private String messageType;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	
	
}
